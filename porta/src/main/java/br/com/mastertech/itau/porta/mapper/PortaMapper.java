package br.com.mastertech.itau.porta.mapper;

import br.com.mastertech.itau.porta.model.Porta;
import br.com.mastertech.itau.porta.model.response.PortaResponse;

public class PortaMapper {

    public static PortaResponse toPortaResponse (Porta porta) {
        PortaResponse portaResponse = new PortaResponse();
        portaResponse.setAndar(porta.getAndar());
        portaResponse.setSala(porta.getSala());
        portaResponse.setId(porta.getId());

        return portaResponse;
    }
}