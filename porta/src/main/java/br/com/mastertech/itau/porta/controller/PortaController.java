package br.com.mastertech.itau.porta.controller;

import br.com.mastertech.itau.porta.mapper.PortaMapper;
import br.com.mastertech.itau.porta.model.Porta;
import br.com.mastertech.itau.porta.model.request.PortaRequest;
import br.com.mastertech.itau.porta.model.response.PortaResponse;
import br.com.mastertech.itau.porta.service.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/porta")
public class PortaController {

    @Autowired
    PortaService portaService;

    @PostMapping
    public ResponseEntity<PortaResponse> create(@Valid @RequestBody PortaRequest portaRequest) {
        Porta porta = new Porta(portaRequest.getSala(), portaRequest.getAndar());
        PortaResponse portaResponse = PortaMapper.toPortaResponse(portaService.cadastrar(porta));
        return new ResponseEntity<PortaResponse>(portaResponse, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public PortaResponse GetById(@PathVariable(value = "id") int id) {
        return PortaMapper.toPortaResponse(portaService.buscar(id));
    }
}
