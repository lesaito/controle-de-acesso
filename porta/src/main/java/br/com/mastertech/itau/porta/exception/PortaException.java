package br.com.mastertech.itau.porta.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Porta nao encontrado")
public class PortaException extends  RuntimeException{
}