package br.com.mastertech.itau.porta.repository;

import br.com.mastertech.itau.porta.model.Porta;
import org.springframework.data.repository.CrudRepository;

public interface PortaRepository extends CrudRepository<Porta, Integer> {

}