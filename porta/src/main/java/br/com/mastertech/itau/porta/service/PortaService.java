package br.com.mastertech.itau.porta.service;

import br.com.mastertech.itau.porta.exception.PortaException;
import br.com.mastertech.itau.porta.model.Porta;
import br.com.mastertech.itau.porta.repository.PortaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PortaService {

    @Autowired
    private PortaRepository portaRepository;

    public Porta cadastrar(Porta porta) {
        return portaRepository.save(porta);
    }

    public Porta buscar(int id) {
        Optional<Porta> porta  = portaRepository.findById(id);
        if (porta.isPresent()) {
            return porta.get();
        } else {
            throw new PortaException();
        }
    }
}
