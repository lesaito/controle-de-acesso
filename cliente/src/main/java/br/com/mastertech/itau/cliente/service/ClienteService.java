package br.com.mastertech.itau.cliente.service;

import br.com.mastertech.itau.cliente.exception.ClienteNotFoundException;
import br.com.mastertech.itau.cliente.model.Cliente;
import br.com.mastertech.itau.cliente.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente cadastrar(Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    public Cliente buscar(int id) {
        Optional<Cliente> cliente  = clienteRepository.findById(id);
        if (cliente.isPresent()) {
            return cliente.get();
        } else {
            throw new ClienteNotFoundException();
        }
    }
}
