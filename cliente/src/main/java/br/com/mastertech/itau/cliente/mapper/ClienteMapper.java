package br.com.mastertech.itau.cliente.mapper;

import br.com.mastertech.itau.cliente.model.Cliente;
import br.com.mastertech.itau.cliente.model.dto.response.ClienteResponse;

public class ClienteMapper {

    public static ClienteResponse toClienteResponse (Cliente cliente) {
        ClienteResponse clienteResponse = new ClienteResponse();
        clienteResponse.setId(cliente.getId());
        clienteResponse.setNome(cliente.getNome());
        return clienteResponse;
    }
}
