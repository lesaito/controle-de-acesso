package br.com.mastertech.itau.cliente.controller;

import br.com.mastertech.itau.cliente.mapper.ClienteMapper;
import br.com.mastertech.itau.cliente.model.Cliente;
import br.com.mastertech.itau.cliente.model.dto.request.CreateClienteRequest;
import br.com.mastertech.itau.cliente.model.dto.response.ClienteResponse;
import br.com.mastertech.itau.cliente.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    ClienteService clienteService;

    @PostMapping
    public ResponseEntity<ClienteResponse> create(@Valid @RequestBody CreateClienteRequest createClienteRequest) {
        Cliente cliente = new Cliente(createClienteRequest.getNome());
        ClienteResponse clienteResponse = ClienteMapper.toClienteResponse(clienteService.cadastrar(cliente));
        return new ResponseEntity<ClienteResponse>(clienteResponse, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ClienteResponse GetById(@PathVariable(value = "id") int id) {
        return ClienteMapper.toClienteResponse(clienteService.buscar(id));
    }
}
