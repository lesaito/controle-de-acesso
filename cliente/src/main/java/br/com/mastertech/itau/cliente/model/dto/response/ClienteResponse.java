package br.com.mastertech.itau.cliente.model.dto.response;

public class ClienteResponse {

    private int id;

    private String nome;


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
