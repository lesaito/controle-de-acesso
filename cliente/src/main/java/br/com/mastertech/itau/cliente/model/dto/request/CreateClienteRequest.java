package br.com.mastertech.itau.cliente.model.dto.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class CreateClienteRequest {

    @NotBlank
    @Size(min = 3)
    private String nome;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
