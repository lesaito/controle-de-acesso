package br.com.mastertech.itau.cliente.repository;

import br.com.mastertech.itau.cliente.model.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {

}