package br.com.mastertech.itau.acesso.mapper;

import br.com.mastertech.itau.acesso.model.Acesso;
import br.com.mastertech.itau.acesso.model.response.AcessoResponse;

public class AcessoMapper {

    public static AcessoResponse toAcessoResponse(Acesso acesso) {
        AcessoResponse acessoResponse = new AcessoResponse();
        acessoResponse.setPorta_id(acesso.getPortaId());
        acessoResponse.setCliente_id(acesso.getClienteId());

        return acessoResponse;
    }
}