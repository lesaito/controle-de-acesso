package br.com.mastertech.itau.acesso.controller;


import br.com.mastertech.itau.acesso.mapper.AcessoMapper;
import br.com.mastertech.itau.acesso.model.request.AcessoRequest;
import br.com.mastertech.itau.acesso.model.response.AcessoResponse;
import br.com.mastertech.itau.acesso.service.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/acesso")
public class AcessoController {

    @Autowired
    AcessoService acessoService;

    @PostMapping
    public ResponseEntity<AcessoResponse> cadastrar (@Valid @RequestBody AcessoRequest acessoRequest) {
        return new ResponseEntity<AcessoResponse>(AcessoMapper.toAcessoResponse(acessoService.cadastrar(acessoRequest)), HttpStatus.OK);
    }

    @DeleteMapping("/{cliente_id}/{porta_id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removerAcesso(@PathVariable(value = "cliente_id") int clienteId, @PathVariable(value = "porta_id") int portaId) {
        acessoService.removerAcesso(clienteId, portaId);
    }

    @GetMapping("/{cliente_id}/{porta_id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<AcessoResponse>  buscarAcesso(@PathVariable(value = "cliente_id") int clienteId, @PathVariable(value = "porta_id") int portaId) {
        return new ResponseEntity<AcessoResponse>(AcessoMapper.toAcessoResponse(acessoService.buscar(clienteId, portaId)), HttpStatus.OK);
    }
}
