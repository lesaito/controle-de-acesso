package br.com.mastertech.itau.acesso.clients;

import br.com.mastertech.itau.acesso.clients.dto.PortaDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "porta")
public interface PortaClient {

    @GetMapping("/porta/{id}")
    public PortaDto buscaPortaPorID(@PathVariable(value = "id") int id);
}