package br.com.mastertech.itau.acesso.repository;

import br.com.mastertech.itau.acesso.model.Acesso;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AcessoRepository extends CrudRepository<Acesso, Integer> {
    public Optional<Acesso> findAcessoByPortaIdAndClienteId(int portaId, int clienteId);

    public void deleteAcessoByPortaIdAndClienteId(int portaId, int clienteId);
}