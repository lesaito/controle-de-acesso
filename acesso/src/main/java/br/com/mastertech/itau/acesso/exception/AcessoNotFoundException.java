package br.com.mastertech.itau.acesso.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Acesso nao encontrado")
public class AcessoNotFoundException extends RuntimeException {
}