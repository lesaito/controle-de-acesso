package br.com.mastertech.itau.acesso.clients;

import br.com.mastertech.itau.acesso.clients.dto.ClienteDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cliente")
public interface ClienteClient {

    @GetMapping("/cliente/{id}")
    public ClienteDto buscaClientePorID(@PathVariable(value = "id") int id);
}
