package br.com.mastertech.itau.acesso.service;

import br.com.mastertech.itau.acesso.clients.ClienteClient;
import br.com.mastertech.itau.acesso.clients.PortaClient;
import br.com.mastertech.itau.acesso.clients.dto.ClienteDto;
import br.com.mastertech.itau.acesso.clients.dto.PortaDto;
import br.com.mastertech.itau.acesso.exception.AcessoNotFoundException;
import br.com.mastertech.itau.acesso.exception.ClienteException;
import br.com.mastertech.itau.acesso.exception.PortaException;
import br.com.mastertech.itau.acesso.model.Acesso;
import br.com.mastertech.itau.acesso.model.request.AcessoRequest;
import br.com.mastertech.itau.acesso.repository.AcessoRepository;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class AcessoService {

    @Autowired
    AcessoRepository acessoRepository;

    @Autowired
    ClienteClient clienteClient;

    @Autowired
    PortaClient portaClient;

    public Acesso cadastrar(AcessoRequest acessoRequest) {
        ClienteDto cliente = null;
        try {
            cliente = clienteClient.buscaClientePorID(acessoRequest.getCliente_id());
        } catch (FeignException.FeignClientException e) {
            throw new ClienteException();
        }

        PortaDto porta = null;
        try {
            porta = portaClient.buscaPortaPorID(acessoRequest.getPorta_id());
        } catch (FeignException.FeignClientException e) {
            throw new PortaException();
        }

        Acesso acesso = new Acesso();
        acesso.setClienteId(cliente.getId());
        acesso.setPortaId(porta.getId());

        return acessoRepository.save(acesso);
    }

    public Acesso buscar (int clienteId, int portaId) {
        ClienteDto cliente = null;
        try {
            cliente = clienteClient.buscaClientePorID(clienteId);
        } catch (FeignException.FeignClientException e) {
            throw new ClienteException();
        }

        PortaDto porta = null;
        try {
            porta = portaClient.buscaPortaPorID(portaId);
        } catch (FeignException.FeignClientException e) {
            throw new PortaException();
        }

        Optional<Acesso> acesso = acessoRepository.findAcessoByPortaIdAndClienteId(porta.getId(), cliente.getId());

        if (acesso.isPresent()) {
            return acesso.get();
        } else {
            throw new AcessoNotFoundException();
        }
    }

    @Transactional
    public void removerAcesso (int clienteId, int portaId) {
        ClienteDto cliente = null;
        try {
            cliente = clienteClient.buscaClientePorID(clienteId);
        } catch (FeignException.FeignClientException e) {
            throw new ClienteException();
        }

        PortaDto porta = null;
        try {
            porta = portaClient.buscaPortaPorID(portaId);
        } catch (FeignException.FeignClientException e) {
            throw new PortaException();
        }

        acessoRepository.deleteAcessoByPortaIdAndClienteId(porta.getId(), cliente.getId());
    }


}